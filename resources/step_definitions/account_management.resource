*** Settings ***
Documentation    Steps definitions for prestashop account management.
Library    Browser
Resource    ../page_objects/site/generic_page_object.resource
Resource    ../page_objects/site/header_page.resource
Resource    ../page_objects/site/login_page.resource
Resource    ../page_objects/site/account_creation_page.resource
Resource    ../page_objects/site/my_account_page.resource
Resource    ../page_objects/site/identity_page.resource


*** Variables ***
&{EXAMPLE_USER}    gender=F
...                first_name=firstname
...                last_name=lastname
...                password=4321pwd
...                email=email@mail.com
...                birth_date=01/01/1990
...                offers=yes
...                policy=yes
...                newsletter=yes
...                gdpr=yes


*** Keywords ***
I am on the AccountCreation page
    [Documentation]    Navigates to the AccountCreation page.
    Go To The SignIn Page
    Go To The AccountCreation Page
    Page Should Be AccountCreation Page

I am logged in
    [Documentation]    Creates a temporary user and goes to the Home page.
    I am on the AccountCreation page
    Create An Example User    ${EXAMPLE_USER}[email]    ${EXAMPLE_USER}[password]
    Go To The Home Page

I am on the MyIdentity page
    [Documentation]    Navigates to the MyIdentity page and checks that the navigation has succeeded.
    Go To The MyAccount Page
    Go To The Identity Page
    Page Should Be MyIdentity Page

I am registered with email "${mail}" and password "${password}"
    [Documentation]    Creates a user with given mail and password and some examples values for
    ...                other account parameters then logs out the newly created user.
    I am on the AccountCreation page
    Create An Example User    ${mail}    ${password}
    I sign out

I am logged out
    [Documentation]    Checks if a user is logged in and if so, logs out the user, then
    ...                checks that the logout process has succeeded.
    ${is_user_logout} =    Is User Logged Out
    IF    "${is_user_logout}" == "${FALSE}"    Sign Out
    User Is Logged Out

I fill accountCreation fields with gender "${gender}" firstName "${first}" lastName "${last}" password "${password}" email "${mail}" birthDate "${birth}" acceptPartnerOffers "${offers}" acceptPrivacyPolicy "${privacy}" acceptNewsletter "${news}" acceptGpdr "${gdpr}" and submit
    [Documentation]    Formats parameters, performs account creation,
    ...                then stores email and password of the newly created user.

    Fill AccountCreation Form  ${gender}    ${first}    ${last}    ${password}    ${mail}
    ...                        ${birth}    ${offers}    ${privacy}    ${news}    ${gdpr}
    Submit AccountCreation Form
    Store Email And Password After Account Creation    ${mail}    ${password}

I fill MyIdentity fields with gender "${gender}" firstName "${first}" lastName "${last}" email "${mail}" newPass "${password}" birthDate "${birth}" partnerOffers "${offers}" privacyPolicy "${privacy}" newsletter "${news}" gpdr "${gdpr}" and submit
    [Documentation]    Formats parameters, performs account modification,
    ...                then stores email and password of the newly modificated user.

    Fill MyIdentity Form    ${gender}    ${first}    ${last}    ${password}    ${mail}
    ...                     ${birth}    ${offers}    ${privacy}    ${news}    ${gdpr}
    Submit MyIdentity Form
    Store Email And Password After Account Modification    ${mail}    ${password}

I sign in with email "${mail}" and password "${password}"
    [Documentation]     Logs a user.

    Go To The SignIn Page
    Fill Login Fields    ${mail}    ${password}
    Submit Login

I sign out
    [Documentation]    Logs out a user.
    Sign Out
    User Is Logged Out

The user firstName "${first_name}" lastName "${last_name}" is connected
    [Documentation]    Checks that a user with given first name and last name is logged in.
    User Is Logged In    ${first_name}    ${last_name}

My personal information is gender "${gender}" firstName "${first}" lastName "${last}" email "${mail}" birthDate "${birth}"
    [Documentation]    Checks that the specified values match the personal information of a logged user.
    Go To The MyAccount Page
    Page Should Be MyAccount Page
    Go To The Identity Page
    Page Should Be MyIdentity Page
    Private Information Should Be  ${gender}    ${first}    ${last}    ${mail}    ${birth}

I still am on the AccountCreation page
    [Documentation]    Checks that the user is still on the AccountCreation Page.
    Page Should Be AccountCreation Page

I still am on the MyIdentityPage
    [Documentation]    Checks that the user is still on the MyIdentity Page.
    Page Should Be MyIdentity Page

Create An Example User
    [Arguments]    ${email}    ${password}
    [Documentation]    Creates a temporary example user.
    Fill AccountCreation Form  ${EXAMPLE_USER}[gender]    ${EXAMPLE_USER}[first_name]    ${EXAMPLE_USER}[last_name]
    ...                        ${password}    ${email}    ${EXAMPLE_USER}[birth_date]    ${EXAMPLE_USER}[offers]
    ...                        ${EXAMPLE_USER}[policy]    ${EXAMPLE_USER}[newsletter]    ${EXAMPLE_USER}[gdpr]
    Submit AccountCreation Form
    User Is Logged In    ${EXAMPLE_USER}[first_name]    ${EXAMPLE_USER}[last_name]
    Store Email And Password After Account Creation    ${email}    ${password}
