*** Settings ***
Documentation    Cannot sign in when email format is not valid
Metadata         ID                           3
Metadata         Automation priority          null
Metadata         Test case importance         Low
Resource         squash_resources.resource
Library          squash_tf.TFParamService
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Cannot sign in when email format is not valid
    [Documentation]    Cannot sign in when email format is not valid

    &{dataset} =    Retrieve Dataset

    Given I am on the AccountCreation page
    When I fill AccountCreation fields with gender "${dataset}[gender]" firstName "${dataset}[first]" lastName "${dataset}[last]" password "${dataset}[password]" email "${dataset}[mail]" birthDate "${dataset}[birth]" acceptPartnerOffers "${dataset}[offers]" acceptPrivacyPolicy "${dataset}[privacy]" acceptNewsletter "${dataset}[news]" acceptGpdr "${dataset}[gpdr]" and submit
    Then I still am on the AccountCreation page
    And An error message is associated with the field "${dataset}[field]"


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_3_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_3_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =        Get Variable Value    ${TEST_SETUP}
    ${TEST_3_SETUP_VALUE} =    Get Variable Value    ${TEST_3_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_3_SETUP_VALUE is not None
        Run Keyword    ${TEST_3_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_3_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_3_TEARDOWN}.

    ${TEST_3_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_3_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =        Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_3_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_3_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END

Retrieve Dataset
    [Documentation]    Retrieves Squash TM's datasets and stores them in a dictionary.
    ...
    ...                For instance, datasets containing 3 parameters "city", "country" and "currency"
    ...                have been defined in Squash TM.
    ...
    ...                First, this keyword retrieves parameter values from Squash TM
    ...                and stores them into variables, using the keyword 'Get Test Param':
    ...                ${city} =    Get Test Param    DS_city
    ...
    ...                Then, this keyword stores the parameters into the &{dataset} dictionary
    ...                with each parameter name as key, and each parameter value as value:
    ...                &{dataset} =    Create Dictionary    city=${city}    country=${country}    currency=${currency}

    ${gender} =      Get Test Param    DS_gender
    ${first} =       Get Test Param    DS_first
    ${last} =        Get Test Param    DS_last
    ${password} =    Get Test Param    DS_password
    ${mail} =        Get Test Param    DS_mail
    ${birth} =       Get Test Param    DS_birth
    ${offers} =      Get Test Param    DS_offers
    ${privacy} =     Get Test Param    DS_privacy
    ${news} =        Get Test Param    DS_news
    ${gpdr} =        Get Test Param    DS_gpdr
    ${field} =       Get Test Param    DS_field

    &{dataset} =    Create Dictionary    gender=${gender}    first=${first}    last=${last}        password=${password}
    ...                                  mail=${mail}        birth=${birth}    offers=${offers}    privacy=${privacy}
    ...                                  news=${news}        gpdr=${gpdr}      field=${field}

    RETURN    &{dataset}
