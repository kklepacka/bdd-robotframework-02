*** Settings ***
Documentation    Cannot write a customize message if message length is too long
Metadata         ID                           13
Metadata         Automation priority          null
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Cannot write a customize message if message length is too long
    [Documentation]    Cannot write a customize message if message length is too long

    &{docstrings} =    Retrieve Docstrings

    Given I am logged out
    When I navigate to category "accessoires"
    And I navigate to product "Mug personnalisable"
    And I customize with message "${docstrings}[docstring_1]"
    Then The customized message is "${docstrings}[docstring_2]"


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_13_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_13_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =        Get Variable Value    ${TEST_SETUP}
    ${TEST_13_SETUP_VALUE} =    Get Variable Value    ${TEST_13_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_13_SETUP_VALUE is not None
        Run Keyword    ${TEST_13_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_13_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_13_TEARDOWN}.

    ${TEST_13_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_13_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =        Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_13_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_13_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END

Retrieve Docstrings
    [Documentation]    Retrieves Squash TM's docstrings and stores them in a dictionary.
    ...
    ...                For instance, two docstrings have been defined in Squash TM,
    ...                the first one containing the string
    ...                "I am the
    ...                FIRST    docstring",
    ...                the second one containing the string "I am the second docstring"
    ...
    ...                First, this keyword retrieves values and converts them to an inline string :
    ...                ${docstring_1} =    Set Variable    I am the\nFIRST\tdocstring"
    ...
    ...                Then, this keyword stores the docstrings into the &{docstrings} dictionary
    ...                with each docstring name as key, and each docstring value as value :
    ...                ${docstrings} =    Create Dictionary    docstring_1=${docstring_1}    docstring_2=${docstring_2}

    ${docstring_1} =    Set Variable    Un très long message qui dépasse les 250 caractères maximum autorisés sur prestashop.\nUn très long message qui dépasse les 250 caractères maximum autorisés sur prestashop.\nUn très long message qui dépasse les 250 caractères maximum autorisés sur prestashop.\nUn très long message qui dépasse les 250 caractères maximum autorisés sur prestashop.
    ${docstring_2} =    Set Variable    Un très long message qui dépasse les 250 caractères maximum autorisés sur prestashop. Un très long message qui dépasse les 250 caractères maximum autorisés sur prestashop. Un très long message qui dépasse les 250 caractères maximum autorisés sur pres

    &{docstrings} =    Create Dictionary    docstring_1=${docstring_1}    docstring_2=${docstring_2}

    RETURN    &{docstrings}
