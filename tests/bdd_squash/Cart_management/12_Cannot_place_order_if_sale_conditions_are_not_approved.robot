*** Settings ***
Documentation    Cannot place order if sale conditions are not approved
Metadata         ID                           12
Metadata         Automation priority          null
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Cannot place order if sale conditions are not approved
    [Documentation]    Cannot place order if sale conditions are not approved

    &{datatables} =    Retrieve Datatables

    Given I am logged in
    And I have in the cart "${datatables}[datatable_1]"
    When I initiate placing order process
    And I fill command form with alias "add1" company "" tva "" address "1 rue du chat" supp "" zip "12345" city "Paris" country "France" phone "" and facturation "yes" and submit
    And I choose delivery "prestashop" and command message ""
    And I pay by paymode "virement bancaire" and choose approveSalesConditions "no"
    Then I still am on the commandPage
    And The submit button is disabled


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_12_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_12_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =        Get Variable Value    ${TEST_SETUP}
    ${TEST_12_SETUP_VALUE} =    Get Variable Value    ${TEST_12_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_12_SETUP_VALUE is not None
        Run Keyword    ${TEST_12_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_12_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_12_TEARDOWN}.

    ${TEST_12_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_12_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =        Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_12_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_12_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END

Retrieve Datatables
    [Documentation]    Retrieves Squash TM's datatables and stores them in a dictionary.
    ...
    ...                For instance, 2 datatables have been defined in Squash TM,
    ...                the first one containing data:
    ...                | name | firstName |
    ...                | Bob  |   Smith   |
    ...                the second one containing data
    ...                | name  | firstName | age |
    ...                | Alice |   Smith   | 45  |
    ...
    ...                First, for each datatable, this keyword retrieves the values of each row
    ...                and stores them in a list, as follows:
    ...                @{row_1_1} =    Create List    name    firstName
    ...
    ...                Then, for each datatable, this keyword creates a list containing all the rows,
    ...                as lists themselves, as follows:
    ...                @{datatable_1} =    Create List    ${row_1_1}    ${row_1_2}
    ...
    ...                Finally, this keyword stores the datatables into the &{datatables} dictionary
    ...                with each datatable name as key, and each datatable list as value :
    ...                &{datatables} =    Create Dictionary    datatable_1=${datatable_1}    datatable_2=${datatable_2}

    @{row_1_1} =    Create List    Product                                     Number    Price
    @{row_1_2} =    Create List    Affiche encadrée The best is yet to come    3         34,80
    @{row_1_3} =    Create List    Illustration vectorielle Renard             2         10,80
    @{datatable_1} =    Create List    ${row_1_1}    ${row_1_2}    ${row_1_3}

    &{datatables} =    Create Dictionary    datatable_1=${datatable_1}

    RETURN    &{datatables}
