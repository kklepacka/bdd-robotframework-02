*** Settings ***
Documentation    Cannot add product to cart if quantity exceeds stock
Metadata         ID                           11
Metadata         Automation priority          null
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Cannot add product to cart if quantity exceeds stock
    [Documentation]    Cannot add product to cart if quantity exceeds stock

    Given I am logged out
    When I navigate to category "accessoires"
    And I navigate to product "Mug Today is a good day"
    And I update quantity to "1599"
    Then The warning message "Il n'y a pas assez de produits en stock." is displayed
    And I cannot add to cart


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_11_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_11_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =        Get Variable Value    ${TEST_SETUP}
    ${TEST_11_SETUP_VALUE} =    Get Variable Value    ${TEST_11_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_11_SETUP_VALUE is not None
        Run Keyword    ${TEST_11_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_11_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_11_TEARDOWN}.

    ${TEST_11_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_11_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =        Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_11_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_11_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END
