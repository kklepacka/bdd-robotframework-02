*** Settings ***
Documentation    Standard placing order logged user
Metadata         ID                           16
Metadata         Automation priority          null
Metadata         Test case importance         Low
Resource         squash_resources.resource
Library          squash_tf.TFParamService
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Standard placing order logged user
    [Documentation]    Standard placing order logged user

    &{dataset} =       Retrieve Dataset
    &{datatables} =    Retrieve Datatables

    Given I am logged in
    And I have in the cart "${datatables}[datatable_1]"
    When I initiate placing order process
    And I fill command form with alias "${dataset}[alias]" company "${dataset}[company]" tva "${dataset}[tva]" address "${dataset}[address]" supp "${dataset}[supp]" zip "${dataset}[zip]" city "${dataset}[city]" country "${dataset}[country]" phone "${dataset}[phone]" and facturation "yes" and submit
    And I choose delivery "${dataset}[delivery]" and command message "${dataset}[message]"
    And I pay by paymode "${dataset}[paymode]" and choose approveSalesConditions "yes"
    And I submit order
    Then The order is placed and contains "${datatables}[datatable_2]"


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_16_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_16_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =        Get Variable Value    ${TEST_SETUP}
    ${TEST_16_SETUP_VALUE} =    Get Variable Value    ${TEST_16_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_16_SETUP_VALUE is not None
        Run Keyword    ${TEST_16_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_16_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_16_TEARDOWN}.

    ${TEST_16_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_16_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =        Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_16_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_16_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END

Retrieve Dataset
    [Documentation]    Retrieves Squash TM's datasets and stores them in a dictionary.
    ...
    ...                For instance, datasets containing 3 parameters "city", "country" and "currency"
    ...                have been defined in Squash TM.
    ...
    ...                First, this keyword retrieves parameter values from Squash TM
    ...                and stores them into variables, using the keyword 'Get Test Param':
    ...                ${city} =    Get Test Param    DS_city
    ...
    ...                Then, this keyword stores the parameters into the &{dataset} dictionary
    ...                with each parameter name as key, and each parameter value as value:
    ...                &{dataset} =    Create Dictionary    city=${city}    country=${country}    currency=${currency}

    ${alias} =       Get Test Param    DS_alias
    ${company} =     Get Test Param    DS_company
    ${tva} =         Get Test Param    DS_tva
    ${address} =     Get Test Param    DS_address
    ${supp} =        Get Test Param    DS_supp
    ${zip} =         Get Test Param    DS_zip
    ${city} =        Get Test Param    DS_city
    ${country} =     Get Test Param    DS_country
    ${phone} =       Get Test Param    DS_phone
    ${delivery} =    Get Test Param    DS_delivery
    ${message} =     Get Test Param    DS_message
    ${paymode} =     Get Test Param    DS_paymode

    &{dataset} =    Create Dictionary    alias=${alias}    company=${company}      tva=${tva}            address=${address}
    ...                                  supp=${supp}      zip=${zip}              city=${city}          country=${country}
    ...                                  phone=${phone}    delivery=${delivery}    message=${message}    paymode=${paymode}

    RETURN    &{dataset}

Retrieve Datatables
    [Documentation]    Retrieves Squash TM's datatables and stores them in a dictionary.
    ...
    ...                For instance, 2 datatables have been defined in Squash TM,
    ...                the first one containing data:
    ...                | name | firstName |
    ...                | Bob  |   Smith   |
    ...                the second one containing data
    ...                | name  | firstName | age |
    ...                | Alice |   Smith   | 45  |
    ...
    ...                First, for each datatable, this keyword retrieves the values of each row
    ...                and stores them in a list, as follows:
    ...                @{row_1_1} =    Create List    name    firstName
    ...
    ...                Then, for each datatable, this keyword creates a list containing all the rows,
    ...                as lists themselves, as follows:
    ...                @{datatable_1} =    Create List    ${row_1_1}    ${row_1_2}
    ...
    ...                Finally, this keyword stores the datatables into the &{datatables} dictionary
    ...                with each datatable name as key, and each datatable list as value :
    ...                &{datatables} =    Create Dictionary    datatable_1=${datatable_1}    datatable_2=${datatable_2}

    @{row_1_1} =    Create List    Product                                     Number    Price
    @{row_1_2} =    Create List    Affiche encadrée The best is yet to come    3         34,80
    @{row_1_3} =    Create List    Illustration vectorielle Renard             2         10,80
    @{datatable_1} =    Create List    ${row_1_1}    ${row_1_2}    ${row_1_3}

    @{row_2_1} =    Create List    Product                                     Number    Price
    @{row_2_2} =    Create List    Affiche encadrée The best is yet to come    3         34,80
    @{row_2_3} =    Create List    Illustration vectorielle Renard             2         10,80
    @{datatable_2} =    Create List    ${row_2_1}    ${row_2_2}    ${row_2_3}

    &{datatables} =    Create Dictionary    datatable_1=${datatable_1}    datatable_2=${datatable_2}

    RETURN    &{datatables}
