*** Settings ***
Documentation    Standard cart product quantity update
Metadata         ID                           14
Metadata         Automation priority          null
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Standard cart product quantity update
    [Documentation]    Standard cart product quantity update

    &{datatables} =    Retrieve Datatables

    Given I am logged out
    And I have in the cart "${datatables}[datatable_1]"
    When I update products quantities to "${datatables}[datatable_2]"
    Then The cart contains "${datatables}[datatable_3]"


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_14_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_14_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =        Get Variable Value    ${TEST_SETUP}
    ${TEST_14_SETUP_VALUE} =    Get Variable Value    ${TEST_14_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_14_SETUP_VALUE is not None
        Run Keyword    ${TEST_14_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_14_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_14_TEARDOWN}.

    ${TEST_14_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_14_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =        Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_14_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_14_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END

Retrieve Datatables
    [Documentation]    Retrieves Squash TM's datatables and stores them in a dictionary.
    ...
    ...                For instance, 2 datatables have been defined in Squash TM,
    ...                the first one containing data:
    ...                | name | firstName |
    ...                | Bob  |   Smith   |
    ...                the second one containing data
    ...                | name  | firstName | age |
    ...                | Alice |   Smith   | 45  |
    ...
    ...                First, for each datatable, this keyword retrieves the values of each row
    ...                and stores them in a list, as follows:
    ...                @{row_1_1} =    Create List    name    firstName
    ...
    ...                Then, for each datatable, this keyword creates a list containing all the rows,
    ...                as lists themselves, as follows:
    ...                @{datatable_1} =    Create List    ${row_1_1}    ${row_1_2}
    ...
    ...                Finally, this keyword stores the datatables into the &{datatables} dictionary
    ...                with each datatable name as key, and each datatable list as value :
    ...                &{datatables} =    Create Dictionary    datatable_1=${datatable_1}    datatable_2=${datatable_2}

    @{row_1_1} =    Create List    Product                                     Number    Price
    @{row_1_2} =    Create List    Affiche encadrée The best is yet to come    5         34,80
    @{row_1_3} =    Create List    Illustration vectorielle Renard             1         10,80
    @{datatable_1} =    Create List    ${row_1_1}    ${row_1_2}    ${row_1_3}

    @{row_2_1} =    Create List    Product                                     Number    Price
    @{row_2_2} =    Create List    Affiche encadrée The best is yet to come    3         34,80
    @{row_2_3} =    Create List    Illustration vectorielle Renard             4         10,80
    @{datatable_2} =    Create List    ${row_2_1}    ${row_2_2}    ${row_2_3}

    @{row_3_1} =    Create List    Product                                     Number    Price
    @{row_3_2} =    Create List    Affiche encadrée The best is yet to come    3         34,80
    @{row_3_3} =    Create List    Illustration vectorielle Renard             4         10,80
    @{datatable_3} =    Create List    ${row_3_1}    ${row_3_2}    ${row_3_3}

    &{datatables} =    Create Dictionary    datatable_1=${datatable_1}    datatable_2=${datatable_2}    datatable_3=${datatable_3}

    RETURN    &{datatables}
